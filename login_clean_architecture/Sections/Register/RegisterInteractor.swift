//
//  RegisterPresenter.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 22/5/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import Foundation
import GIGLibrary

protocol RegisterInteractorInput {
    var output: RegisterInteractorOutput? {get set}

    func saveUser(user: User)
}

protocol RegisterInteractorOutput {
    func signupDidFinish(user: User?)
}

class RegisterInteractor: RegisterInteractorInput {
    
    // MARK: - Interactor output
    let datamanager: DataManager
    var output: RegisterInteractorOutput?
    
    init(datamanager: DataManager) {
        self.datamanager = datamanager
    }
    
    func saveUser(user: User) {
        if self.datamanager.store(user: user) {
            self.output?.signupDidFinish(user: user)
        } else {
            self.output?.signupDidFinish(user: nil)
        }
    }
}
