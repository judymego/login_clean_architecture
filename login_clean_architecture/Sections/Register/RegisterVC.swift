//
//  RegisterVC.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 22/5/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit
import GIGLibrary

class RegisterVC: UIViewController {
    
    @IBOutlet weak var signupButton: MainButton!
    @IBOutlet weak var viewForm: UIView!
    
    @IBOutlet weak var nameTextField: FormTextField!
    @IBOutlet weak var lastnameTextField: FormTextField!
    @IBOutlet weak var passwordTextField: FormTextField!
    @IBOutlet weak var repeatPasswordTextField: FormTextField!
    @IBOutlet weak var emailTextField: FormTextField!
    
    // MARK: - Attributtes
    
    var presenter: RegisterPresenter?

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SIGN UP"
    }
    
    @IBAction func signupTapped(_ sender: Any) {
    
        let userViewModel = UserViewModel(
            email: self.emailTextField.text,
            password: self.passwordTextField.text,
            repeatPassword: self.repeatPasswordTextField.text,
            name: self.nameTextField.text,
            lastname: self.lastnameTextField.text)
        
        self.presenter?.signupUser(userViewModel: userViewModel)
    }
}

extension RegisterVC: Instantiable {
    
    // MARK: - Instantiable
    
    public static func storyboard() -> String {
        return "Register"
    }
    
    public static func identifier() -> String? {
        return "RegisterVC"
    }
}

extension RegisterVC: RegisterUI {
    // Show toast with an icon
    func showAlert(message: String) {
        let alert = Alert(title: "Clean", message: message)
        alert.addDefaultButton("OK", usingAction: nil)
        alert.show()
    }
}



