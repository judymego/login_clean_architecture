//
//  RegisterPresenter.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 22/5/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import Foundation

protocol RegisterUI: class {
    func showAlert(message: String)
}

protocol RegisterPresenterInput {
    func signupUser(userViewModel: UserViewModel)
}

class RegisterPresenter {
    
    // MARK: - Public attributes
    
    weak var view: RegisterUI?
    let wireframe: AppWireframe
    
    // MARK: - Interactors
    
    var interactor: RegisterInteractorInput
    
    // MARK: - Input methods
    
    init(view: RegisterUI, wireframe: AppWireframe, interactor: RegisterInteractorInput) {
        self.view = view
        self.wireframe = wireframe
        self.interactor = interactor
        self.interactor.output = self

    }

}

extension RegisterPresenter: RegisterPresenterInput {
   
    func signupUser(userViewModel: UserViewModel) {
        do {
            let user = try userViewModel.createUser()
            self.interactor.saveUser(user: user)
        } catch UserViewModel.InputError.InputMissing(let fields) {
            print("FIELDS: \(fields)")
            self.view?.showAlert(message: kLocaleErrorUserviewmodelInputMissing)
        } catch UserViewModel.InputError.EmailIncorrect {
            self.view?.showAlert(message: kLocaleErrorUserviewmodelEmailIncorrect)
        } catch UserViewModel.InputError.PasswordNotEqual {
            self.view?.showAlert(message: kLocaleErrorUserviewmodelPasswordNotEqual)
        } catch {
            self.view?.showAlert(message: kLocaleErrorGeneralUnknow)
        }
    }
}

extension RegisterPresenter: RegisterInteractorOutput {
    func signupDidFinish(user: User?) {
        if let userRegistered = user {
            self.view?.showAlert(message: "\(userRegistered.name) has been registered")
            self.wireframe.popViewController()
        }
    }
}
