//
//  AppWireframe.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 29/3/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit
import GIGLibrary

protocol MainWireframeInput {
    func showAppLaunchWireframe()
    func showRegisterVC()
    func popViewController()
    func dissmiss(completion: (() -> Void)?)
}

class AppWireframe: MainWireframeInput {
    
    var window: UIWindow?
    
    private var navigationController = UINavigationController()

    func showAppLaunchWireframe() {
        let loginWireframe = LoginWireframe(
            navigationController: self.navigationController,
            mainWireframe: self
        )
    
        guard let loginVC = loginWireframe.showLogin() else {
            LogWarn("loginVC not found")
            return
        }
        
        self.navigationController.setViewControllers([loginVC], animated: false)
        window?.rootViewController = self.navigationController
    }
    

    func showRegisterVC() {
        
        guard let viewController = try? Instantiator<RegisterVC>().viewController() else { return }
        
        let interactor = RegisterInteractor(datamanager: DataManager())
        let presenter = RegisterPresenter(
            view: viewController,
            wireframe: self,
            interactor: interactor
        )
        viewController.presenter = presenter
        self.navigationController.show(viewController, sender: nil)
    }
    
    func popViewController() {
        self.navigationController.popViewController(animated: true)
    }
    
    // MARK: UIViewController
    
    func dissmiss(completion: (() -> Void)? = nil) {
        self.topViewController()?.dismiss(animated: true, completion: completion)
    }
    
    // MARK: - Private Helpers
    
    private func topViewController() -> UIViewController? {
        var rootVC = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedController = rootVC?.presentedViewController {
            rootVC = presentedController
        }
        
        return rootVC
    }
}
