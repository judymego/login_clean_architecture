//
//  DataManager.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 23/5/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit

enum BBDDKey {
    case users
}

class DataManager {
    
    var userDefaults: UserDefaults

    convenience init() {
        self.init(userDefaults: UserDefaults.standard)
    }
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func store(user: User) -> Bool {
        var users: [User]
        
        if var usersDB = self.userDefaults.object(forKey: "users") as? [User],
            usersDB.count > 0 {
            usersDB.append(user)
            users = usersDB
        } else {
            users = [User]()
            users.append(user)
        }
        
        return self.archivedObject(object: users, key: "users")
    }
    
    func fetchUser(with email: String) -> User? {
        guard let users = self.unarchiveObject(key: "users") as? [User] else {
            return nil
        }
        let user = users.filter { $0.email == email }.first
        return user
    }
    
    func fetchUsers() -> [User]? {
        guard let users = self.unarchiveObject(key: "users") as? [User] else {
            return nil
        }
        return users
    }
    
    func removeUser(with email: String) -> Bool {
        if let usersDB = self.fetchUsers() {
            let users = usersDB.filter { $0.email != email }
            return self.archivedObject(object: users, key: "users")
        }
        return false
    }
    
    
    // MARK: - PRIVATE 
    
    func archivedObject(object: Any, key: String) -> Bool {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }
    
    func unarchiveObject(key: String) -> Any? {
        if let data = self.userDefaults.object(forKey: key) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data)
        } else {
            return nil
        }
    }
    
}
