//
//  User.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 29/3/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit

struct PropertyKey {
    static let name     = "name"
    static let lastname = "lastname"
    static let email    = "email"
    static let password = "password"
}

class User: NSObject, NSCoding {

    let name: String
    let lastname: String?
    let email: String
    let password: String
    
    init(name: String, lastname: String?, email: String, password: String) {
        self.name = name
        self.lastname = lastname
        self.email = email
        self.password = password
    }
    
    //MARK: NSCoding

    required convenience init?(coder aDecoder: NSCoder) {
        guard   let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String,
                let email = aDecoder.decodeObject(forKey: PropertyKey.email) as? String,
                let password = aDecoder.decodeObject(forKey: PropertyKey.password) as? String else {
                    print("some params are nil")
                    return nil
        }
        let lastname = aDecoder.decodeObject(forKey: PropertyKey.lastname) as? String
        self.init(name: name, lastname: lastname, email: email, password: password)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: PropertyKey.email)
        aCoder.encode(password, forKey: PropertyKey.password)
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(lastname, forKey: PropertyKey.lastname)
    }

    
}
