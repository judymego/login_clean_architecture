//
//  UserViewModel.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 23/5/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit

class UserViewModel {
    let email: String?
    let password: String?
    let repeatPassword: String?
    let name: String?
    let lastname: String?
    
    var fields = [String]()
    // Errors 
    
    enum InputError: Error {
        case InputMissing(fields: [String])
        case EmailIncorrect
        case PasswordNotEqual
    }
    
    // MARK: - PUBLIC
    
    init(email: String?, password: String?, repeatPassword: String?, name: String?, lastname: String?) {
        self.email = email
        self.password = password
        self.repeatPassword = repeatPassword
        self.name = name
        self.lastname = lastname
    }

    func createUser() throws -> User {

        guard let password = password, !self.fieldEmpty(field: self.name, key: "name")
            , let repeatPassword = repeatPassword,
            let name = name, let email = email
        else {
            throw InputError.InputMissing(fields: fields)
        }
        
        if !isValidEmail(testStr: email) {
            throw InputError.EmailIncorrect
        }
        
        if !compare(p1: password, p2: repeatPassword) {
            throw InputError.PasswordNotEqual
        }
        
        return User(name: name, lastname: lastname, email: email, password: password)
    }
    
    // MARK: - PRIVATE
    
    // IMPROVEMENT
    private func fieldEmpty(field: String?, key: String) -> Bool {
        if field?.characters.count == 0 {
            self.fields.append(key)
            return true
        }
        return false
    }
    
    private func compare(p1: String, p2: String) -> Bool {
        if p1 == p2 {
            return true
        }
        return false
    }
    
    private func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
}
