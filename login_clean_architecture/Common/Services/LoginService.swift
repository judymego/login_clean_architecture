//
//  LoginService.swift
//  CleanArchitecture
//
//  Created by Judith Medina on 29/3/17.
//  Copyright © 2017 Judith Medina. All rights reserved.
//

import UIKit
import GIGLibrary


struct LoginService {
    
    let datamanager = DataManager()

    func signIn(username: String, password: String) -> User? {
        if let user = self.datamanager.fetchUser(with: username) {
            return user
        }
        
        return nil
    }
}
