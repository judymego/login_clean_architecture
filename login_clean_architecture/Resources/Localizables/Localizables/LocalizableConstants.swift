/*
LocalizableConstants.swift

GENERATED - DO NOT MODIFY - use localio instead.

Created by localio
*/

import Foundation


let kLocaleErrorUserviewmodelInputMissing: String = { return NSLocalizedString("error_userviewmodel_input_missing", comment: "") }()
let kLocaleErrorUserviewmodelEmailIncorrect: String = { return NSLocalizedString("error_userviewmodel_email_incorrect", comment: "") }()
let kLocaleErrorUserviewmodelPasswordNotEqual: String = { return NSLocalizedString("error_userviewmodel_password_not_equal", comment: "") }()
let kLocaleErrorLoginCredentialsnil: String = { return NSLocalizedString("error__login_credentialsnil", comment: "") }()
let kLocaleErrorLoginCredentialsempty: String = { return NSLocalizedString("error__login_credentialsempty", comment: "") }()
let kLocaleErrorLoginInvalidcredentials: String = { return NSLocalizedString("error__login_invalidcredentials", comment: "") }()
let kLocaleErrorGeneralUnknow: String = { return NSLocalizedString("error__general_unknow", comment: "") }()
